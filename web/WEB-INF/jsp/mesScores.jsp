<%--
  Created by IntelliJ IDEA.
  User: baptp
  Date: 26/10/2019
  Time: 18:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Score de ${session_userBean.getLogin()}</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <%-- CSS --%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mesScores.css"/>

    <%--    FONT--%>
    <link href="https://fonts.googleapis.com/css?family=Caveat:700&display=swap" rel="stylesheet">

    <script src="https://kit.fontawesome.com/d047199128.js" crossorigin="anonymous"></script>


</head>
<body>

<div class="menu">
    <a href="<c:url value="/home" />">
    <button type="button" class="bt bt-secondary">
        <i class="fas fa-arrow-circle-left"></i>
    </button></a>
</div>


<div class="container-fluid">
    <div class="row h-100">


        <div class="col-5 m-auto text-center">
            <img class="logo" src="${pageContext.request.contextPath}/image/scores.png">


            <a class="btnjouer" href="<c:url value="/question" />">
                AMELIORER MON SCORE !
            </a>

            <table class="table text-center" >
                <thead class="thead-dark">
                <tr>

                    <th scope="col">Rank</th>
                    <th scope="col">Score</th>
                </tr>
                </thead>
                <tbody>
                <% int i = 0; %>
                <c:forEach var="item" items="${mespartieBean.listMesParties}" varStatus="loop">
                    <% i++; %>
                    <tr>
                        <% if (i == 1){%>
                        <th scope="row"><i style="color: #FFD700" class="fas fa-trophy"></i><% out.print("  "+ i); %></th>
                        <%}else if (i== 2){%>
                        <th scope="row"><i style="color: #c0c0c0" class="fas fa-trophy"></i><% out.print("  "+ i); %></th>
                        <%}else if (i== 3){%>
                        <th scope="row"><i style="color: #cd5832" class="fas fa-trophy"></i><% out.print("  "+ i); %></th>
                        <%}else{%>
                        <th scope="row"><i style="color: transparent" class="fas fa-trophy"></i><% out.print("  "+ i); %></th>
                        <%}%>
                        <td>${item.getScore()}</td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>

        </div>
    </div>
</div>


</body>
</html>
