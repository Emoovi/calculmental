<%--
  Created by IntelliJ IDEA.
  User: alois
  Date: 24/10/2019
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <%-- CSS --%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/question.css"/>

    <%--    FONT--%>
    <link href="https://fonts.googleapis.com/css?family=Caveat:700&display=swap" rel="stylesheet">

    <title>Question</title>
</head>
<body>
<div class="container-fluid">
    <div class="row h-100">
        <div class="col-10 m-auto">
            <jsp:useBean id="partieBean" class="model.PartieBean" scope="request"/>
            <div class="mx-auto text-center " style="width: 100%">
                <img class="questionImg" src="${pageContext.request.contextPath}/image/question.png">
                <p>Question ${partieBean.position_courante}/10 (résultat à arrondir au centièmes) :</p>
                <form class="form-inline d-flex justify-content-center" method="post" action="question">
                    <p class="laQuestion"><b></b>${partieBean.calcul_courant} =</p>
                    <input type="number" step="0.01" class="form-control leform" name="form-answer" required>
                    <div class="col-12">
                        <button class="btn btn-outline-dark" type="submit">Question Suivante</button>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</html>
