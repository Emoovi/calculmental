<%@ page import="controller.HomeController" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>home</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <%-- CSS --%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/home.css"/>

    <script src="https://kit.fontawesome.com/d047199128.js" crossorigin="anonymous"></script>

</head>
<body>
<jsp:useBean id="partieBean" class="model.PartieBean" scope="request"/>



<div class="container-fluid containpage">
    <div class="row h-100">
        <div class="col-12 m-auto">
            <img class="logo" src="${pageContext.request.contextPath}/image/logo.PNG">
            <div class="btn-group menu">
                <button type="button" class="bt bt-secondary  " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <div class="row">
                    <div class="col-8 contentUser">
                    <div class="col-12">
                    ${session_userBean.getLogin()}
                   </div>
                    <div class="col-12">
                        <p class="maxscore">MAX SCORE : ${top1}</p>
                    </div>
                   </div>
                    <div class="col-4 contImage">
                        <img class="userimg" src="${pageContext.request.contextPath}/image/user.png">
                    </div>
                   </div>
                </button>

                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<c:url value="/mesScores" />">Voir ses scores</a>
                    <a class="dropdown-item" href="<c:url value="/home/deco" />">Déconnexion</a>
                </div>
            </div>


                <div class="btnCalcul" >

                    <a  href="<c:url value="/question" />" >
                    <div class="rectangle h-100">
                        <h1 class="m-auto">JOUER UNE PARTIE</h1>
                    </div>
                    <div class="triangle">

                    </div>
                    </a>
                </div>


            <div class="col-lg-8 .col-md-11 containTable">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>

                        <th scope="col">Rank</th>
                        <th scope="col">Pseudo</th>
                        <th scope="col">Score</th>
                    </tr>
                    </thead>
                    <tbody>
                    <% int i = 0; %>
                    <c:forEach var="item2" items="${partieBean.listParties}" varStatus="loop">
                        <% i++; %>
                        <tr>
                            <% if (i == 1){%>
                            <th scope="row"><i style="color: #FFD700" class="fas fa-trophy"></i><% out.print("  "+ i); %></th>
                            <%}else if (i== 2){%>
                            <th scope="row"><i style="color: #c0c0c0" class="fas fa-trophy"></i><% out.print("  "+ i); %></th>
                            <%}else if (i== 3){%>
                            <th scope="row"><i style="color: #cd5832" class="fas fa-trophy"></i><% out.print("  "+ i); %></th>
                            <%}else{%>
                            <th scope="row"><i style="color: transparent" class="fas fa-trophy"></i><% out.print("  "+ i); %></th>
                            <%}%>
                            <td>${item2.getUser().getLogin()}</td>
                            <td>${item2.getScore()}</td>
                        </tr>
                    </c:forEach>

                    </tbody>
                </table>
            </div>
            <div class="col-2"></div>
        </div>
    </div>


</div>





</body>
</html>
