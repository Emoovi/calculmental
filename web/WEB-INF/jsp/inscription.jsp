<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Inscription</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <%-- CSS --%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/inscription.css"/>

    <%--    FONT--%>
    <link href="https://fonts.googleapis.com/css?family=Caveat:700&display=swap" rel="stylesheet">


</head>
<body>
<div class="container-fluid">
    <div class="row h-100">
        <div class="col-5 m-auto card text-center lacard">
            <h1 class="title text-center my-4">Inscription</h1>
            <jsp:useBean id="userBean" class="model.UserBean" scope="request"/>
            <c:if test="${ !empty userBean.authResult }">
                <div class="callout alert">
                    <p>${ userBean.authResult }</p>
                </div>
            </c:if>
            <c:if test="${ !userBean.isConnected( pageContext.request ) }">
                <div class="col-12">
                    <form method="post" action="inscription">
                        <input type="text" class="input form-control my-3" placeholder="Nom" name="form-lastname" required>
                        <input type="text" class="input form-control my-3" placeholder="Prénom" name="form-firstname" required>
                        <input type="password" class="input form-control my-3" placeholder="Mot de Passe"
                               name="form-password" required>
                        <input type="password" class="input form-control my-3" placeholder="Confirmation Mot de Passe"
                               name="form-password2" required>
                        <button class="buttonInscr" type="submit">Valider</button>
                    </form>
                </div>
            </c:if>
        </div>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</html>
