<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Resultat</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <%-- CSS --%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/home.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/resultat.css"/>

</head>
<body>
<jsp:useBean id="CalculBean" class="model.CalculBean" scope="request"/>



<div class="container-fluid containpage">
    <div class="row h-100">
        <div class="col-12 m-auto">
            <img class="logo" src="${pageContext.request.contextPath}/image/logo.PNG">

            <div class="btnCalcul" >

                <a  href="<c:url value="/home"/>" >
                    <div class="rectangle h-100">
                        <h1 class="m-auto">Retourner au Menu</h1>
                    </div>
                    <div class="triangle">

                    </div>
                </a>
            </div>
            <div class="col-lg-8 .col-md-11 containTable">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Position</th>
                        <th scope="col">Calcul</th>
                        <th scope="col">Réponse</th>
                        <th scope="col">Votre Réponse</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="item" items="${CalculBean.listResultat}" varStatus="loop">
                        <c:choose>
                            <c:when test="${(item.getReponse() != item.getReponse_utilisateur())}">
                                <tr class="resultTableauRouge">
                            </c:when>
                            <c:otherwise>
                                <tr class="resultTableauVert">
                            </c:otherwise>
                        </c:choose>
                            <td>${item.getPosition()}</td>
                            <td>${item.getCalcul()}</td>
                            <td>${item.getReponse()}</td>
                            <td>${item.getReponse_utilisateur()}</td>
                        </tr>
                    </c:forEach>

                    </tbody>
                </table>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>
</body>
</html>
