package dal;

import bo.Calcul;
import model.PartieBean;

import java.sql.SQLException;
import java.util.List;

public interface ICalculDAO<ID,E> extends IDAO<ID,E> {
    public List<Calcul> findAllBy_Partie(PartieBean partie) throws SQLException;
    public void create_List(List<Calcul> listcalc) throws SQLException;
}
