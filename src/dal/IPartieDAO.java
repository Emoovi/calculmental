package dal;

import bo.Partie;
import bo.User;
import model.UserBean;

import java.sql.SQLException;
import java.util.List;

public interface IPartieDAO<ID,E> extends IDAO<ID,E> {
    public List<Partie> getTop10() throws SQLException;
    public List<Partie> mesParties(UserBean user) throws SQLException;
    public int getTop1(UserBean user) throws SQLException;
    public Partie getLastInsert(UserBean user) throws SQLException;

}
