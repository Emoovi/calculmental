package dal.jdbc;

import bo.User;
import dal.DAOFactory;
import dal.IUserDAO;

import java.sql.*;
import java.util.List;

public class UserDAO implements IUserDAO<Long, User> {

//    private static final String AUTHENT_QUERY = "SELECT * FROM utilisateur";
    private static final String AUTHENT_QUERY = "SELECT * FROM utilisateur WHERE login = ? AND password = ?";
    private static final String INSERT_USER_QUERY = "INSERT INTO utilisateur (login,password) VALUES (?,?)";
    private static final String FIND_BY_ID_QUERY = "SELECT * FROM utilisateur WHERE id = ?";

    //Connexion utilisateur à partir d'un login et du mot de passe
    @Override
    public User authenticate( String login, String password ) throws SQLException {
        Connection connection = DAOFactory.getJDBCConnection();

        User user = null;
        if ( null != connection ) {

            try ( PreparedStatement ps = connection
                    .prepareStatement( AUTHENT_QUERY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE ) ) {

                ps.setString( 1, login );
                ps.setString( 2, password );
                try ( ResultSet rs = ps.executeQuery() ) {
                    if ( rs.next() ) {
                        user = new User();
                        user.setId(rs.getInt("id"));
                        user.setLogin( rs.getString( "login" ) );
                        user.setPassword( rs.getString( "password" ) );
                    }
                }
            }
        }
        return user;
    }

    //Création utilisateur
    @Override
    public void create(User user) throws SQLException {
        Connection connection = DAOFactory.getJDBCConnection();
        if ( null != connection ) {
            try (PreparedStatement ps = connection
                    .prepareStatement( INSERT_USER_QUERY, Statement.RETURN_GENERATED_KEYS ) ) {
                ps.setString( 1, user.getLogin() );
                ps.setString( 2, user.getPassword() );
                ps.executeUpdate();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        user.setId(rs.getInt(1));
                    }
                }
            }
        }
    }

    @Override
    public void update(User object) throws SQLException {

    }

    @Override
    public void delete(User object) throws SQLException {

    }

    //Recherche par ID
    @Override
    public User findById(Long aLong) throws SQLException {
        Connection connection = DAOFactory.getJDBCConnection();

        User user = null;
        if ( null != connection ) {
            try ( PreparedStatement ps = connection
                    .prepareStatement( FIND_BY_ID_QUERY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE ) ) {

                ps.setLong( 1, aLong);
                try ( ResultSet rs = ps.executeQuery() ) {
                    if ( rs.next() ) {
                        user = new User();
                        user.setId(rs.getInt("id"));
                        user.setLogin( rs.getString( "login" ) );
                        user.setPassword( rs.getString( "password" ) );
                    }
                }
            }
        }
        return user;
    }

    @Override
    public List<User> findByAll() throws SQLException {
        return null;
    }
}
