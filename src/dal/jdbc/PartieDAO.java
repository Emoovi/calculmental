package dal.jdbc;

import bo.Partie;
import bo.User;
import dal.DAOFactory;
import dal.IDAO;
import dal.IPartieDAO;
import model.UserBean;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PartieDAO implements IPartieDAO<Long, Partie> {
    private static final String INSERT_PARTIE_QUERY = "INSERT INTO partie (id_utilisateur, score) VALUES (?,?)";
    private static final String FIND_TOP_10_QUERY = "SELECT * FROM partie p INNER JOIN utilisateur u ON u.id = p.id_utilisateur ORDER BY p.score DESC LIMIT 10";
    private static final String FIND_MES_PARTIES = "SELECT * FROM partie p INNER JOIN utilisateur u ON u.id = p.id_utilisateur where p.id_utilisateur = ? ORDER BY p.score DESC LIMIT 10";
    private static final String FIND_BY_ID_QUERY = "SELECT * FROM partie WHERE id =?";
    private static final String FIND_TOP_1_QUERY = "SELECT * FROM partie WHERE id_utilisateur = ? order by score DESC LIMIT 1 ";
    private static final String FIND_LAST_INSERT_BY_USER = "SELECT * FROM partie WHERE id_utilisateur = ? ORDER BY id DESC  LIMIT 1";
    private static final String UPDATE_QUERY = "UPDATE partie SET id_utilisateur=?,score=? WHERE id=?";

    //Création Partie
    @Override
    public void create(Partie partie) throws SQLException {
        Connection connection = DAOFactory.getJDBCConnection();
        if ( null != connection ) {
            try (PreparedStatement ps = connection
                    .prepareStatement( INSERT_PARTIE_QUERY, Statement.RETURN_GENERATED_KEYS ) ) {
                ps.setInt( 1, partie.getUser().getId() );
                ps.setInt( 2, partie.getScore() );
                ps.executeUpdate();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        partie.setId(rs.getInt(1));
                    }
                }
            }
        }
    }

    //Modification Partie
    @Override
    public void update(Partie object) throws SQLException {
        Connection connection = DAOFactory.getJDBCConnection();
        if(connection != null){
            try(PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY)){
                ps.setInt(1,object.getUser().getId());
                ps.setInt(2,object.getScore());
                ps.setInt(3,object.getId());
                ps.executeUpdate();
            }
        }
    }

    @Override
    public void delete(Partie object) throws SQLException {

    }

    //Recherche par ID
    @Override
    public Partie findById(Long aLong) throws SQLException {
        String  idstring = aLong.toString();
        Partie partie = new Partie();
        int id = Integer.parseInt(idstring);
        Connection connection = DAOFactory.getJDBCConnection();
        UserDAO userDAO = new UserDAO();
        if(connection != null){
            try(PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_QUERY)){
                ps.setInt(1,id);
                try(ResultSet rs = ps.executeQuery()){
                    while (rs.next()){
                        partie.setId(rs.getInt("id"));
                        partie.setScore(rs.getInt("score"));
                        partie.setUser( userDAO.findById(rs.getLong("id_utilisateur")));
                    }
                }
            }

        }
        return partie;
    }

    @Override
    public List<Partie> findByAll() throws SQLException {
        return null;
    }

    //Recherche les 10 meilleurs scores
    @Override
    public List<Partie> getTop10() throws SQLException {
        List<Partie> listPartie = new ArrayList<>();
        Connection connection = DAOFactory.getJDBCConnection();
        if (connection != null) {

            try (PreparedStatement ps = connection.prepareStatement(FIND_TOP_10_QUERY)) {
                try (ResultSet rs = ps.executeQuery()) {

                    while (rs.next()) {

                        Partie partie = new Partie();
                        partie.setId(rs.getInt("id"));
                        Long idUser = rs.getLong("id_utilisateur");
                        User user = new User(rs.getString("login"), rs.getString("password"));
                        partie.setUser(user);
                        partie.setScore(rs.getInt("score"));
                        listPartie.add(partie);
                    }
                }
            }
        }
        return listPartie;
    }

    //Recherche le meilleur score d'un utilisateur
    @Override
    public int getTop1(UserBean user) throws SQLException {
        int reponse = 0;
        Connection connection = DAOFactory.getJDBCConnection();
        if(connection != null){
            try(PreparedStatement ps = connection.prepareStatement(FIND_TOP_1_QUERY)){
                ps.setInt( 1, user.getId() );
                try(ResultSet rs = ps.executeQuery()){
                    if (rs.next()){
                        reponse = rs.getInt("score");
                    }
                }
            }
        }
        return reponse;
    }

    //Recherche les 10 meilleurs partie d'un utilisateur
    @Override
    public List<Partie> mesParties(UserBean userBean) throws SQLException {
        List<Partie> listPartie = new ArrayList<>();
        Connection connection = DAOFactory.getJDBCConnection();
        if (connection != null) {

            try (PreparedStatement ps = connection.prepareStatement(FIND_MES_PARTIES)) {

                ps.setInt( 1, userBean.getId() );
                try (ResultSet rs = ps.executeQuery()) {

                    while (rs.next()) {

                        Partie partie = new Partie();
                        partie.setId(rs.getInt("id"));
                        Long idUser = rs.getLong("id_utilisateur");
                        User user = new User(rs.getString("login"), rs.getString("password"));
                        partie.setUser(user);
                        partie.setScore(rs.getInt("score"));
                        listPartie.add(partie);
                    }
                }
            }
        }
        return listPartie;
    }

    //Recherche la dernière partie inséré par un utilisateur
    @Override
    public Partie getLastInsert(UserBean user) throws SQLException {
        Partie partie = new Partie();
        Connection connection = DAOFactory.getJDBCConnection();
        if(connection !=  null){
            try(PreparedStatement ps = connection.prepareStatement(FIND_LAST_INSERT_BY_USER)){
                ps.setInt(1,user.getId());
                try(ResultSet rs = ps.executeQuery()){
                    if(rs.next()){
                        partie.setId(rs.getInt("id"));
                        partie.setScore(rs.getInt("score"));
                    }
                }
            }
        }
        return partie;
    }
}
