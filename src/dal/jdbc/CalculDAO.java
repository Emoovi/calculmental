package dal.jdbc;

import bo.Calcul;
import dal.DAOFactory;
import dal.ICalculDAO;
import model.PartieBean;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CalculDAO implements ICalculDAO<Long, Calcul> {

    private static final String INSERT_CALCUL_QUERY = "INSERT INTO calcul (id_partie, calcul, reponse, reponse_utilisateur, position) VALUES (?,?,?,?,?)";
    private static final String SELECT_BY_IDPARTIE_QUERY = "SELECT * FROM calcul WHERE id_partie =?";

    //Create l'ensemble des calculs d'une partie
    public void create_List(List<Calcul> calcul) throws SQLException {

        Connection connection = DAOFactory.getJDBCConnection();
        if (null != connection) {
             for (Calcul item : calcul) {
                 try (PreparedStatement ps = connection.prepareStatement( INSERT_CALCUL_QUERY, Statement.RETURN_GENERATED_KEYS ) ) {
                     ps.setInt(1, item.getPartie());
                     ps.setString(2, item.getCalcul());
                     ps.setDouble(3, item.getReponse());
                     ps.setDouble(4, item.getReponse_utilisateur());
                     ps.setInt(5, item.getPosition());
                     ps.executeUpdate();
                 }
             }
        }
    }

    @Override
    public void create(Calcul object) throws SQLException {

    }

    @Override
    public void update(Calcul object) throws SQLException {

    }

    @Override
    public void delete(Calcul object) throws SQLException {

    }

    @Override
    public Calcul findById(Long aLong) throws SQLException {
        return null;
    }

    @Override
    public List<Calcul> findByAll() throws SQLException {
        return null;
    }

    //Retourne la liste de Calcul d'une partie
    @Override
    public List<Calcul> findAllBy_Partie(PartieBean partie) throws SQLException {
        List<Calcul> ListeCalculs = new ArrayList<>();
        int id_partie = partie.getId();
        Connection connection = DAOFactory.getJDBCConnection();
        if (connection != null) {
            try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_IDPARTIE_QUERY)) {
                ps.setInt(1, id_partie);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        Calcul calcul = new Calcul();
                        calcul.setId(rs.getInt("id"));
                        calcul.setCalcul(rs.getString("calcul"));
                        calcul.setPosition(rs.getInt("position"));
                        calcul.setReponse(rs.getDouble("reponse"));
                        calcul.setReponse_utilisateur(rs.getDouble("reponse_utilisateur"));
                        calcul.setPartie(rs.getInt("id_partie"));
                        ListeCalculs.add(calcul);
                    }
                }
            }
        }
        return ListeCalculs;
    }


}
