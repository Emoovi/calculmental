package dal;

import dal.jdbc.CalculDAO;
import dal.jdbc.PartieDAO;
import dal.jdbc.UserDAO;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.SQLException;

public class DAOFactory {

    private static String mode;
    private static String dsName;
    private static Connection connection;

    private DAOFactory() {}

    public static void init( ServletContext servletContext ) throws NamingException {

                mode = servletContext.getInitParameter( "DS_MODE" );
                dsName = servletContext.getInitParameter( "DS_NAME" );
                // lecture du contexte JDNI de notre servlet
                Context context = new InitialContext();
                // lecture de la datasource définie par requête JNDI
                DataSource ds = (DataSource) context.lookup(dsName);
                try {
                    connection = ds.getConnection();
                } catch (SQLException e){
                    e.printStackTrace();
                }

    }

    public static Connection getJDBCConnection() throws SQLException {
        return connection;
    }

    public static IUserDAO getUserDAO() {
        IUserDAO dao;
        dao = new UserDAO();
        return dao;
    }

    public static IPartieDAO getPartieDAO() {
        IPartieDAO dao;
        dao = new PartieDAO();
        return dao;
    }

    public static ICalculDAO getCalculDAO() {
        ICalculDAO dao;
        dao = new CalculDAO();
        return dao;
    }

    public static IPartieDAO getTopDixDAO() {
        IPartieDAO dao;
        dao = new PartieDAO();
        return dao;
    }
    public static IPartieDAO getTop1DAO() {
        IPartieDAO dao;
        dao = new PartieDAO();
        return dao;
    }
    public static ICalculDAO getListResultat(){
        ICalculDAO dao;
        dao = new CalculDAO();
        return dao;
    }

    public static IPartieDAO mesParties() {
        IPartieDAO dao;
        dao = new PartieDAO();
        return dao;
    }

    public static IPartieDAO create() {
        IPartieDAO dao;
        dao = new PartieDAO();
        return dao;
    }

}
