package model;

import bo.User;
import dal.DAOFactory;
import utilitaire.Sha256;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public class UserBean implements Serializable {

    // Constantes

    private static final String FORM_FIELD_LOGIN = "form-username";
    private static final String FORM_FIELD_PWD = "form-password";

    private static final String FORM_FIELD_FIRSTNAME = "form-firstname";
    private static final String FORM_FIELD_LASTNAME = "form-lastname";
    private static final String FORM_FIELD_PASSWORD = "form-password";
    private static final String FORM_FIELD_PASSWORD2 = "form-password2";

    private static final String ATT_SESSION_IS_CONNECTED = "isConnected";

    /* Attributs */

    private int id;
    private String login;
    private String password;
    private String authResult;

    /* Constructeurs */

    public UserBean() {
        id= 0;
        login = "";
        password = "";
        authResult = "";
    }

    public UserBean(int id, String login, String password){
        this.id= id;
        this.login = login;
        this.password = password;
    }

    /* Getter & Setter */

    public int getId() {return id; }

    public void setId(int id) { this.id = id; }

    public String getLogin() {
        return login;
    }

    public void setLogin( String login ) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword( String password ) {
        this.password = password;
    }

    public String getAuthResult() {
        return authResult;
    }

    public void setAuthResult( String authResult ) {
        this.authResult = authResult;
    }

    // Méthodes

    public boolean isConnected( HttpServletRequest request ) {
        HttpSession session = request.getSession();
        User isConnected = ( User ) session.getAttribute( ATT_SESSION_IS_CONNECTED );
        return isConnected != null;
    }

    public void authenticate( HttpServletRequest request ) {

        HttpSession session = request.getSession();
        UserBean bean = (UserBean) session.getAttribute("session_userBean");

        login = request.getParameter( FORM_FIELD_LOGIN );
        password = request.getParameter( FORM_FIELD_PWD );

        try {
            Sha256 sha256 = new Sha256();
            password = sha256.toHexString(sha256.getSHA(password));
        } catch (NoSuchAlgorithmException e) {
            authResult = "Veuillez utiliser des caractères ASCII.";
        }

        User user = null;
        try {
            user = DAOFactory.getUserDAO().authenticate(login, password);
            try {
                UserBean beanvalid = new UserBean(user.getId(), user.getLogin(), user.getPassword());
                session.setAttribute("userBean", beanvalid);
            }catch(Exception e){
                authResult = "Identification échouée, merci de recommencer...";
            }


            if ( null != user ) {
                request.getSession().setAttribute(ATT_SESSION_IS_CONNECTED, user);
            }else{
                user = new User(login, password);
                authResult = "Identification échouée, merci de recommencer...";
            }
        }catch ( SQLException e ) {
            authResult = "Identification échouée : Pb de connexions à la base de données !";
        }
    }

    public void signup( HttpServletRequest request ) {

        String firstname = request.getParameter( FORM_FIELD_FIRSTNAME );
        String lastname = request.getParameter( FORM_FIELD_LASTNAME );
        password  = request.getParameter( FORM_FIELD_PASSWORD );
        String password2 = request.getParameter( FORM_FIELD_PASSWORD2 );

        if(password.equals(password2)){

            try {
                login = firstname + "." + lastname;

                Sha256 sha256 = new Sha256();
                password = sha256.toHexString(sha256.getSHA(password));

            } catch (NoSuchAlgorithmException e) {
                authResult = "Veuillez utiliser des caractères ASCII.";
            }

            User user = new User(login, password);

            request.getSession().setAttribute(ATT_SESSION_IS_CONNECTED, user);

            try {
                DAOFactory.getUserDAO().create(user);
            } catch (SQLException e) {
                authResult = "Inscription échouée : Pb de connexions à la base de données !";
            }
        }else{
            authResult = "Veuillez renseigner deux mots de passes identiques.";
        }
    }
}