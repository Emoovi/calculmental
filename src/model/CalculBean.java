package model;

import bo.Calcul;
import dal.DAOFactory;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CalculBean  implements Serializable {

    /* Attributs */

    private int id;
    private int id_partie;
    private String calcul;
    private Double reponse;
    private Double reponse_utilisateur;
    private int position;

    public List<Calcul> listCalculs = null;
    public List<Calcul> listResultat = null;
    public int top1;

    /* Constructeurs */

    public CalculBean() { }

    public CalculBean(int id, int id_partie, String calcul, Double reponse, Double reponse_utilisateur, int position) {
        this.id = id;
        this.id_partie = id_partie;
        this.calcul = calcul;
        this.reponse = reponse;
        this.reponse_utilisateur = reponse_utilisateur;
        this.position = position;
    }

    /* Getter & Setter */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_partie() {
        return id_partie;
    }

    public void setId_partie(int id_partie) {
        this.id_partie = id_partie;
    }

    public String getCalcul() {
        return calcul;
    }

    public void setCalcul(String calcul) {
        this.calcul = calcul;
    }

    public Double getReponse() {
        return reponse;
    }

    public void setReponse(Double reponse) {
        this.reponse = reponse;
    }

    public Double getReponse_utilisateur() {
        return reponse_utilisateur;
    }

    public void setReponse_utilisateur(Double reponse_utilisateur) {
        this.reponse_utilisateur = reponse_utilisateur;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<Calcul> getListCalculs() {
        return listCalculs;
    }

    public void setListCalculs(List<Calcul> listCalculs) {
        this.listCalculs = listCalculs;
    }

    public List<Calcul> getListResultat() {
        return listResultat;
    }

    public void setListResultat(List<Calcul> listResultat) {
        this.listResultat = listResultat;
    }

    public int getTop1() {
        return top1;
    }

    public void setTop1(int top1) {
        this.top1 = top1;
    }

    // Méthodes

    public List<Calcul> listResultat(PartieBean partie) {
        try {
            listResultat = DAOFactory.getListResultat().findAllBy_Partie(partie);
        } catch (SQLException e) {
            System.out.println("Une erreur est survenue lors de la connexion a la BDD");
        }
        return listResultat;
    }

    public int top1(UserBean user){
        try {
            top1 = DAOFactory.getTop1DAO().getTop1(user);
        }catch (SQLException e) {
            System.out.println("Une erreur est survenue lors de la connexion a la BDD");
        }
        return top1;
    }
}
