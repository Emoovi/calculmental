package model;

import bo.Calcul;
import bo.Partie;
import bo.User;
import dal.ICalculDAO;
import dal.jdbc.UserDAO;
import dal.DAOFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.sql.Array;
import java.sql.SQLException;
import java.util.*;

public class PartieBean  implements Serializable {

    private static final String FORM_FIELD_ANSWER = "form-answer";

    private int id;
    private User idUser;
    private double score;
    private int classement;

    public List<Partie> listParties = null;
    public List<Partie> listMesParties = null;
    public int top1;

    public String calcul_courant;
    public Double reponse_courante;
    public Double reponse_utilisateur_courante;
    public int position_courante;

    public List<Calcul> listdesCalculs;



    public PartieBean() {
        this.id = 0;
        this.idUser = new User();
        this.score = 0;
        this.classement = 0;

        this.calcul_courant = "";
        this.reponse_courante = 0.0;
        this.position_courante = 1;
        this.listdesCalculs = new ArrayList<Calcul>();

    }

    public PartieBean(int id, User idUser, double score, int classement) {
        this.id = id;
        this.idUser = idUser;
        this.score = score;
        this.classement = classement;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getIdUser() {
        return idUser;
    }

    public void setIdUser(User idUser) {
        this.idUser = idUser;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getClassement() {
        return classement;
    }

    public void setClassement(int classement) {
        this.classement = classement;
    }

    public List<Partie> getListParties() {
        return listParties;
    }

    public void setListParties(List<Partie> listParties) {
        this.listParties = listParties;
    }

    public List<Partie> getListMesParties() {
        return listMesParties;
    }

    public void setListMesParties(List<Partie> listMesParties) {
        this.listMesParties = listMesParties;
    }

    public int getTop1() { return top1; }

    public void setTop1(int top1) { this.top1 = top1; }

    public String getCalcul_courant() { return calcul_courant; }

    public void setCalcul_courant(String calcul_courant) { this.calcul_courant = calcul_courant; }

    public Double getReponse_courante() { return reponse_courante; }

    public void setReponse_courante(Double reponse_courante) { this.reponse_courante = reponse_courante; }

    public Double getReponse_utilisateur_courante() { return reponse_utilisateur_courante; }

    public void setReponse_utilisateur_courante(Double reponse_utilisateur_courante) { this.reponse_utilisateur_courante = reponse_utilisateur_courante; }

    public int getPosition_courante() { return position_courante; }

    public void setPosition_courante(int position_courante) { this.position_courante = position_courante; }

    public List<Calcul> getListdesCalculs() {
        return listdesCalculs;
    }

    public void setListdesCalculs(List<Calcul> listdesCalculs) {
        this.listdesCalculs = listdesCalculs;
    }

    /* -------------- Méthodes -------------- */

    public List<Partie> topDix(){
        try {
            listParties = DAOFactory.getTopDixDAO().getTop10();

        } catch (SQLException e) {
            System.out.println("Une erreur est survenue lors de la connexion a la BDD");
        }
        return listParties;

    }

    public int top1(UserBean user){
        try {
            top1 = DAOFactory.getTop1DAO().getTop1(user);
        }catch (SQLException e) {
            System.out.println("Une erreur est survenue lors de la connexion a la BDD");
        }
        return top1;
    }

    public List<Partie> mesParties(UserBean userBean){

        id = userBean.getId();

        try {

            listMesParties = DAOFactory.mesParties().mesParties(userBean);

        } catch (SQLException e) {
            System.out.println("Une erreur est survenue lors de la connexion a la BDD");
        }
        return listMesParties;
    }

    public void addToList(HttpServletRequest request){
        HttpSession session = request.getSession();
        PartieBean bean = (PartieBean) session.getAttribute("session_partieBean");
        UserBean ubean = (UserBean) session.getAttribute("session_userBean");
        Double answer = Double.parseDouble(request.getParameter( FORM_FIELD_ANSWER ));



        int idpartie;

        if(bean.id == 0){

            User user = new User(ubean.getId(), ubean.getLogin(), ubean.getPassword());
            Partie partie = new Partie(user, 0);

            try {
                DAOFactory.getPartieDAO().create(partie);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Partie partie_insert = new Partie();

            try {
                partie_insert = DAOFactory.getPartieDAO().getLastInsert(ubean);
                bean.setId(partie_insert.getId());

            } catch (SQLException e) {
                e.printStackTrace();
            }

            idpartie = partie_insert.getId();
        }else{
            idpartie = bean.id;
        }


        Calcul calcul = new Calcul(idpartie, bean.calcul_courant, bean.reponse_courante, answer, bean.position_courante);
//        if (bean.position_courante == 1){
//            List<Calcul> laList = Arrays.asList(calcul);
//            listdesCalculs = laList;
//        }else {
            listdesCalculs.add(calcul);
//        }
        bean.setPosition_courante(bean.getPosition_courante() + 1);

        if(bean.reponse_courante.toString().equals(answer.toString()) ){
           // Partie partie = new Partie();
            bean.setScore(bean.getScore() + 1);
//            try {
//                Long indentifiant = Long.parseLong(String.valueOf(idpartie));
//                partie = (Partie) DAOFactory.getPartieDAO().findById(indentifiant);
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            partie.setScore(partie.getScore() + 1);
//            try {
//                DAOFactory.getPartieDAO().update(partie);
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
        }


        session.setAttribute("session_partieBean", bean);
        if(bean.position_courante == 11){
            saveCalcul(request, listdesCalculs);
        }
    }

    public void saveCalcul( HttpServletRequest request, List<Calcul> listcalc ){
        HttpSession session = request.getSession();
        PartieBean bean = (PartieBean) session.getAttribute("session_partieBean");
        UserBean ubean = (UserBean) session.getAttribute("session_userBean");


        try {
            DAOFactory.getCalculDAO().create_List(listcalc);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        bean.setPosition_courante(bean.getPosition_courante() + 1);
        session.setAttribute("session_partieBean", bean);
    }
}
