package bo;

public class User {

    /* Attributs */

    private int id;
    private String login;
    private String password;

    /* Constructeurs */

    public User() {}

    public User( String login, String password ) {
        this.login = login;
        this.password = password;
    }

    public User(int id, String login, String password ) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

    /* Getter & Setter */

    public int getId() {
        return id;
    }

    public void setId( int id ) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin( String login ) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword( String password ) {
        this.password = password;
    }
}
