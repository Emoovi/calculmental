package bo;

public class Calcul {

    /* Attributs */

    private int id;
    private int id_partie;
    private String calcul;
    private Double reponse;
    private Double reponse_utilisateur;
    private int position;

    /* Constructeurs */

    public Calcul() {}

    public Calcul(int id_partie, String calcul, Double reponse, Double reponse_utilisateur, int position) {
        this.id_partie = id_partie;
        this.calcul = calcul;
        this.reponse = reponse;
        this.reponse_utilisateur = reponse_utilisateur;
        this.position = position;
    }

    public Calcul(int id, int id_partie, String calcul, Double reponse, Double reponse_utilisateur, int position) {
        this.id = id;
        this.id_partie = id_partie;
        this.calcul = calcul;
        this.reponse = reponse;
        this.reponse_utilisateur = reponse_utilisateur;
        this.position = position;
    }

    /* Getter & Setter */

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public int getId_partie() { return id_partie; }

    public void setId_partie(int id_partie) { this.id_partie = id_partie; }

    public int getPartie() { return id_partie; }

    public void setPartie(int id_partie) { this.id_partie = id_partie; }

    public String getCalcul() { return calcul; }

    public void setCalcul(String calcul) { this.calcul = calcul; }

    public Double getReponse() { return reponse; }

    public void setReponse(Double reponse) { this.reponse = reponse; }

    public Double getReponse_utilisateur() { return reponse_utilisateur; }

    public void setReponse_utilisateur(Double reponse_utilisateur) { this.reponse_utilisateur = reponse_utilisateur; }

    public int getPosition() { return position; }

    public void setPosition(int position) { this.position = position; }
}

