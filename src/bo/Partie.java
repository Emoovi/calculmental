package bo;

public class Partie {

    /* Attributs */

    private int id;
    private User user;
    private int score;

    /* Constructeurs */

    public Partie() {}

    public Partie(User user, int score) {
        this.user = user;
        this.score = score;
    }

    public Partie(int id, User user, int score) {
        this.id = id;
        this.user = user;
        this.score = score;
    }

    /* Getter & Setter */

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
