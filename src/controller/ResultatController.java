package controller;

import bo.Partie;
import model.CalculBean;
import model.PartieBean;
import model.UserBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/resultat")
public class ResultatController extends HttpServlet {
    private static final String PAGE_RESULTAT = "/WEB-INF/jsp/resultat.jsp";
    private static final String PAGE_HOME = "/home";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        CalculBean bean = new CalculBean();
        PartieBean pbean = (PartieBean) session.getAttribute("session_partieBean");
        bean.listResultat = bean.listResultat(pbean);
        req.setAttribute("partieBean", pbean);
        req.setAttribute("CalculBean",bean);
        UserBean ubean = (UserBean) session.getAttribute("session_userBean");
        req.setAttribute("top1",pbean.top1(ubean));
        session.setAttribute("session_partieBean", new PartieBean());
        req.getServletContext().getRequestDispatcher( PAGE_RESULTAT ).forward( req, resp );
    }
}
