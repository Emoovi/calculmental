package controller;

import model.PartieBean;
import model.UserBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(urlPatterns = {"/mesScores"})
public class MesScoresControlleur extends HttpServlet{

    private static final String PAGE_MES_SCORES = "/WEB-INF/jsp/mesScores.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        session.getAttribute("session_userBean");
        UserBean userBean;
        userBean = (UserBean) session.getAttribute("session_userBean");

        PartieBean mespartieBean = new PartieBean();
        mespartieBean.mesParties(userBean);
        req.setAttribute("mespartieBean", mespartieBean);


        req.getServletContext().getRequestDispatcher( PAGE_MES_SCORES ).forward( req, resp );

    }
}
