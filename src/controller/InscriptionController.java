package controller;

import model.UserBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet(urlPatterns = {"/inscription"})
public class InscriptionController extends HttpServlet {

    private static final String PAGE_INSCRIPTION = "/WEB-INF/jsp/inscription.jsp";
    private static final String PAGE_HOME = "/home";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        UserBean bean = ( UserBean ) req.getAttribute( "userBean" );

        if ( bean == null ) {
            bean = new UserBean();
            req.setAttribute( "userBean", bean );
        }
        if ( bean.isConnected( req ) ) {
            resp.sendRedirect( req.getContextPath() + PAGE_HOME );
        } else {
            req.getServletContext().getRequestDispatcher( PAGE_INSCRIPTION ).forward( req, resp );
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        UserBean bean = new UserBean();
        bean.signup( req );
        req.setAttribute( "userBean", bean );
        doGet( req, resp );
    }
}
