package controller;

import bo.Partie;
import bo.User;
import dal.DAOFactory;
import model.PartieBean;
import model.UserBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import static java.lang.Math.sqrt;

@WebServlet("/question")
public class QuestionController extends HttpServlet {

    private static final String PAGE_QUESTION = "/WEB-INF/jsp/question.jsp";
    private static final String PAGE_RESULT = "/resultat";
    private static final String PAGE_HOME = "/home";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        PartieBean bean = ( PartieBean ) req.getAttribute( "partieBean" );
        if ( bean == null ) {
            bean = new PartieBean();
        }

        PartieBean session_bean = (PartieBean) session.getAttribute("session_partieBean");
        if(session_bean == null) {
            session.setAttribute("session_partieBean", new PartieBean());
            session_bean = (PartieBean) session.getAttribute("session_partieBean");
        }

        /* 10 Questions puis résultat */
        if(session_bean.getPosition_courante() <= 10){

            /* Génération du calcul et de sa réponse */
            String calcul = CalculGenerator();
            Double reponse = reponseGenerator(calcul);

            bean.setId(session_bean.getId());
            bean.setCalcul_courant(calcul);
            bean.setReponse_courante(reponse);
            bean.setPosition_courante(session_bean.getPosition_courante());

            req.setAttribute( "partieBean", bean );
            session.setAttribute("session_partieBean", bean);

            req.getServletContext().getRequestDispatcher( PAGE_QUESTION ).forward( req, resp );

        }else{
//            Long indentifiant = Long.parseLong(String.valueOf(bean.getId()));
            UserBean session_user = (UserBean) session.getAttribute("session_userBean");
            Partie partie = new Partie();
            User user = new User();
            user.setId(session_user.getId());
            user.setLogin(session_user.getLogin());
            user.setPassword(session_user.getPassword());

            partie.setUser(user);
            partie.setScore((int)bean.getScore());
            partie.setId(bean.getId());
//            try {

//                partie = (Partie) DAOFactory.getPartieDAO().findById(indentifiant);
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }

            try {
                DAOFactory.getPartieDAO().update(partie);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            resp.sendRedirect( req.getContextPath() + PAGE_RESULT );
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        PartieBean bean = (PartieBean) session.getAttribute("session_partieBean");
        bean.addToList( req );
        req.setAttribute( "partieBean", bean );
        doGet( req, resp );
    }

    // Génération aléatoire du calcul
    private String CalculGenerator(){

        String calcul ="";
        String operateur []= {"+","/","-","*"};
        String numtamp = "";
        String operateur2 [] = {"inv","rac"};
        int j = 0;

        // On génère le tableau d'opérateur
        while(numtamp ==""){
            for( int i=0; i<4; i++ ){
                int numrandom =  0 + (int)(Math.random() * ((1 - 0) + 1));
                if(numrandom == 1){
                    int numerorandom =  0 + (int)(Math.random() * ((3 - 0) + 1));
                    numtamp += operateur[numerorandom]+":";
                }
            }
        }
        String taboperateur[] = numtamp.split(":");
        String operateurfinal [] = new String[taboperateur.length];

        for(int i=0; i <= taboperateur.length-1;i++){
            operateurfinal[j] = taboperateur[i];
            j++;
        }

        //On génère le premier nombre
        int num =  1 + (int)(Math.random() * ((100 - 1) + 1));
        calcul += num;

        // On génère les opérateurs et les nombres qui suivent
        for(int i=0; i <= (operateurfinal.length)-1;i++){
            int random =  1 + (int)(Math.random() * ((3 - 1) + 1));

            //Si le random est égale à 1 on utilise un opérateur particulier (Inv et Rac) sinon on utilise les opérateurs classiques
            if( random == 1){
                int randombis =  0 + (int)(Math.random() * ((1 - 0) + 1));
                int num2 = 1 + (int)(Math.random() * ((100 - 1) + 1));
                calcul +=" "+ operateurfinal[i]+" "+operateur2[randombis]+"("+num2+")";
            }else{
                int num2 = 1 + (int)(Math.random() * ((100 - 1) + 1));
                calcul +=" "+ operateurfinal[i]+" "+num2;
            }
        }
       return calcul;
    }

    //On calcul la réponse du calcul générer avant
    private Double reponseGenerator(String calcul) {
        int j =0;
        int k =0;
        Double reponse = 0.00;
        String tableau [] = calcul.split(" ");

        //On commence par regarder les INV et RAC
        for (int i=0; i <= tableau.length-1;i++){
            if(tableau[i].contains("inv")){

                //On enlève les parenthèse pour n'avoir que le nombre
                String concat = "\\(";
                String concat2 = "\\)";
                String tableauSplit [] = tableau[i].split(concat);
                String valeur [] = tableauSplit[1].split(concat2);
                String valeurfinal = valeur[0];
                String tableauTamp [] = tableau;

                //On réalise l'inverse
                double ajout = 1/(Double.parseDouble(valeurfinal));
                tableauTamp[i] = ""+ajout;
                tableau = tableauTamp;
            }else if(tableau[i].contains("rac") ){
                String tableauTamp2 [] = tableau;

                //On enlève les parenthèse pour n'avoir que le nombre
                String concat = "\\(";
                String concat2 = "\\)";
                String tableauSplit [] = tableau[i].split(concat);
                String valeur [] = tableauSplit[1].split(concat2);
                String valeurfinal = valeur[0];
                // on réalise la racine
                var ajout2 = sqrt(Integer.parseInt(valeurfinal));
                tableauTamp2[i] = ""+ajout2;
                tableau = tableauTamp2;
            }
        }

        //On fait attention aux opérateurs prioritaires (* et /)
        for (int i=0; i <= tableau.length-1;i++){
            j = k =0;
            if(tableau[i].equals("*")){
                String tableauTamp [] = tableau;

                //Si la case du tableau avant et après sont vides l'opération passe alors à la case suivante
                while (tableau[i-1+j]==""){
                    j--;
                }
                while (tableau[i+1+k]==""){
                    k++;
                }
                double ajout4=(Double.parseDouble(tableau[i-1+j]))*(Double.parseDouble(tableau[i+1+k]));
                tableauTamp[i+1+k] = ""+ajout4;
                tableauTamp[i-1+j]="";
                tableauTamp[i]="";
                tableau = tableauTamp;
            }else if(tableau[i].equals("/")){
                String tableauTamp2 [] = tableau;

                //Si la case du tableau avant et après sont vides l'opération passe alors à la case suivante
                while (tableau[i-1+j]==""){
                    j--;
                }
                while (tableau[i+1+k]==""){
                    k++;
                }
                double ajout3 = (Double.parseDouble(tableau[i-1+j]))/(Double.parseDouble(tableau[i+1+k]));
                tableauTamp2[i+1+k] = ""+ajout3;
                tableauTamp2[i-1+j]="";
                tableauTamp2[i]="";
                tableau = tableauTamp2;
            }
        }

        //On finit par faire les additions et soustraction
        for (int i=0; i <= tableau.length-1;i++){
            j = k =0;
            if(tableau[i].equals("+")){
                String tableauTamp [] = tableau;

                //Si la case du tableau avant et après sont vides l'opération passe alors à la case suivante
                while (tableau[i-1+j]==""){
                    j--;
                }
                while (tableau[i+1+k]==""){
                    k++;
                }
                double test1 =(Double.parseDouble(tableau[i-1+j]))+(Double.parseDouble(tableau[i+1+k]));
                tableauTamp[i+1+k] = ""+ test1;
                tableauTamp[i-1+j]="";
                tableauTamp[i]="";
                tableau = tableauTamp;
            }else if(tableau[i].equals("-")){
                String tableauTamp2 [] = tableau;

                //Si la case du tableau avant et après sont vides l'opération passe alors à la case suivante
                while (tableau[i-1+j]==""){
                    j--;
                }
                while (tableau[i+1+k]==""){
                    k++;
                }
                double test = (Double.parseDouble(tableau[i-1+j]))-(Double.parseDouble(tableau[i+1+k]));
                tableauTamp2[i+1+k] = ""+ test;
                tableauTamp2[i-1+j]="";
                tableauTamp2[i]="";
                tableau = tableauTamp2;
            }
        }
        for(int i = 0; i<=tableau.length-1; i++){
            if(tableau[i] !=""){
                reponse = Double.parseDouble(tableau[i]);
            }
        }

        reponse = (int)(Math.round(reponse * 100))/100.0;;
        return reponse;
    }
}
