package controller;

import model.UserBean;

import javax.jws.soap.SOAPBinding;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Console;
import java.io.IOException;
import java.util.logging.Level;

@WebServlet(urlPatterns = {"/connexion"})
public class ConnexionController extends HttpServlet {

    private static final String PAGE_CONNEXION = "/WEB-INF/jsp/connexion.jsp";
    private static final String PAGE_HOME = "/home";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        UserBean bean = ( UserBean ) req.getAttribute( "userBean" );

        if ( bean == null ) {
            bean = new UserBean();
            req.setAttribute( "userBean", bean );
            session.setAttribute("session_userBean", bean);
        }
        if ( bean.isConnected( req ) ) {
            session.setAttribute("session_userBean", session.getAttribute("userBean"));
            resp.sendRedirect( req.getContextPath() + PAGE_HOME );
        } else {
            req.getServletContext().getRequestDispatcher( PAGE_CONNEXION ).forward( req, resp );
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        UserBean bean = new UserBean();
        bean.authenticate( req );
        req.setAttribute( "userBean", bean );
        doGet( req, resp );
    }
}
