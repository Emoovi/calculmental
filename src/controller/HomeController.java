package controller;

import model.PartieBean;
import model.UserBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebServlet(urlPatterns = {"/home", "/home/deco"})
public class HomeController extends HttpServlet {

    private static final String PAGE_HOME = "/WEB-INF/jsp/home.jsp";
    private static final String PAGE_CONNEXION = "/connexion";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        PartieBean bean = new PartieBean();
        bean.topDix();
        req.setAttribute("partieBean", bean);
        UserBean ubean = (UserBean) session.getAttribute("session_userBean");
        req.setAttribute("top1",bean.top1(ubean));

        session.getAttribute("session_userBean");


        String path = req.getServletPath();
        String realPath = path.substring( path.lastIndexOf( "/" )+ 1 );
        switch ( realPath ) {
            // Déconnexion
            case "deco":
                UserBean userBean = new UserBean();
                session.setAttribute("session_userBean", null);
                session.setAttribute("userBean", null);
                req.setAttribute("userBean", null);
                session.invalidate();
                resp.sendRedirect( req.getContextPath() + PAGE_CONNEXION );
                break;

            // Connexion => Home
            case "home":
            default:
                req.getServletContext().getRequestDispatcher(PAGE_HOME).forward(req, resp);


        }

    }


}
