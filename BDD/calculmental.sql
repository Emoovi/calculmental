-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.7.26 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour calculmental
CREATE DATABASE IF NOT EXISTS `calculmental` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `calculmental`;

-- Listage de la structure de la table calculmental. calcul
CREATE TABLE IF NOT EXISTS `calcul` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_partie` int(10) unsigned NOT NULL,
  `calcul` varchar(255) NOT NULL,
  `reponse` double NOT NULL,
  `reponse_utilisateur` double NOT NULL,
  `position` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_calcul_partie` (`id_partie`),
  CONSTRAINT `FK_calcul_partie` FOREIGN KEY (`id_partie`) REFERENCES `partie` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Listage des données de la table calculmental.calcul : ~9 rows (environ)
/*!40000 ALTER TABLE `calcul` DISABLE KEYS */;
INSERT INTO `calcul` (`id`, `id_partie`, `calcul`, `reponse`, `reponse_utilisateur`, `position`) VALUES
	(2, 10, '98 / 449 * 776', 169.37193763919822, 300, 1),
	(3, 12, '244 / 652 + 442', 442.37423312883436, -63.7844155844, 2),
	(4, 15, '307 + 69 + 50', 426, -63.7844155844, 1),
	(5, 16, '575 / rac(100) / inv(465) + 732 - inv(238)', 969.8763440860215, -63.7844155844, 2),
	(6, 18, '66 - rac(67)', 57.81464722812755, 57.814647, 2),
	(7, 19, '62 + 37 - 60 + 15', 54, 12, 1),
	(8, 20, '100 / rac(43) * 21', 320.2469976984698, 23, 1),
	(9, 21, '37 / 29', 1.28, 34, 1),
	(10, 22, '38 - inv(100) - 6', 132, 575, 1);
/*!40000 ALTER TABLE `calcul` ENABLE KEYS */;

-- Listage de la structure de la table calculmental. partie
CREATE TABLE IF NOT EXISTS `partie` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(10) unsigned NOT NULL,
  `score` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_partie_utilisateur` (`id_utilisateur`),
  CONSTRAINT `FK_partie_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Listage des données de la table calculmental.partie : ~22 rows (environ)
/*!40000 ALTER TABLE `partie` DISABLE KEYS */;
INSERT INTO `partie` (`id`, `id_utilisateur`, `score`) VALUES
	(1, 11, 0),
	(2, 11, 0),
	(3, 11, 0),
	(4, 11, 0),
	(5, 11, 0),
	(6, 11, 0),
	(7, 11, 0),
	(8, 11, 0),
	(9, 11, 0),
	(10, 11, 0),
	(11, 11, 0),
	(12, 11, 0),
	(13, 11, 0),
	(14, 11, 0),
	(15, 11, 0),
	(16, 11, 0),
	(17, 11, 0),
	(18, 11, 0),
	(19, 11, 0),
	(20, 11, 0),
	(21, 11, 0),
	(22, 11, 0);
/*!40000 ALTER TABLE `partie` ENABLE KEYS */;

-- Listage de la structure de la table calculmental. utilisateur
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Listage des données de la table calculmental.utilisateur : ~4 rows (environ)
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` (`id`, `login`, `password`) VALUES
	(1, 'alois.moreau', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17'),
	(10, 'alo.moreau', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17'),
	(11, 'simon.rethore', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17'),
	(12, 'baptiste.papa', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17');
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
